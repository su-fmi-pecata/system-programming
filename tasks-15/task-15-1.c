#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <stdlib.h>

int main() 
{ 
    int j  =7;
    if( fork() == 0) {
        j++;
        exit(0);
        if(execlp("who", "who", 0) == -1) {
            j = 7;
        } else {
            printf("\n Stojnostta na j = %d", j);
        }
    } else {
        --j;
        printf("\nStojnostta na j = %d", j);
    }

	return 0; 
}

