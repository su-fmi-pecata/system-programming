# System Programming

Repo for course about system programming in FMI

Taks are provided by Bocheva, so most of them
cannot be directly or easily compiled.


# Base functions


## [Open](https://linux.die.net/man/2/open)

Given a pathname for a file, open() returns a file descriptor.

```cpp
#include<sys/types.h>
#include<sys/stat.h>
#include <fcntl.h>

int open(const char *pathname, int flags);
int open(const char *pathname,int flags, mode_t mode); 

int main() {

    int fd = open("baz.txt", O_WRONLY | O_CREAT, 0644);
    if(fd < 0) {
        return 1;
    }
    
    return 0;
}
```

### Flags:
- `O_RDONLY`: read only;
- `O_WRONLY`: write only;
- `O_RDWR`: read and write;
- `O_CREAT`: create file if it doesn’t exist;
- `O_EXCL`: prevent creation if it already exists;
- `O_APPEND`: append to file;
- `O_TRUNC`: If the file already exists and is a regular file and the open mode allows writing (i.e., is `O_RDWR` or `O_WRONLY`) it will be truncated to length 0


| Return value ||
| ------- | - |
| success | a file descriptor for the opened file |
| error   | -1 and sets errno |




## [Creat](https://linux.die.net/man/2/open)

`creat()` is equivalent to `open()` with flags equal to `O_CREAT|O_WRONLY|O_TRUNC`.

```cpp
#include <fcntl.h>

int creat(const char *pathname, mode_t mode);

int main() {

    int fd = creat(fn, S_IRUSR | S_IWUSR);
    if(fd < 0) {
        return 1;
    }

    return 0;
}
```

| Return value ||
| ------- | - |
| success | a file descriptor for the opened file |
| error   | -1 and sets errno |




## [Close](https://linux.die.net/man/2/close)

closes a file descriptor

```cpp
#include <unistd.h>

int close(int fd);

int main() {

    close(2);

    return 0;
}
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |

## [Read](https://linux.die.net/man/2/read)

`read()` attempts to read up to count bytes from file descriptor fd into the buffer starting at buf.

```cpp
#include <unistd.h>

ssize_t read(int fd, void *buf, size_t count);


int main() {
    const int BUFFER_SIZE = 64;
    char buffer[BUFFER_SIZE];

    int fd = open("sample.txt", O_RDONLY);
    if(fd < 0) {
        return 1;
    }

    int readSize = read(fd, &buffer, BUFFER_SIZEf);

    return 0;
}
```

| Return value ||
| ------- | - |
| success | the number of bytes read is (zero indicates end of file). The file position is advanced by this number. |
| error   | -1 and sets errno |




## [Write](https://linux.die.net/man/2/write)

write to a file descriptor

```cpp
#include <unistd.h>

ssize_t write(int fd, const void *buf, size_t count);


int main() {
    const int BUFFER_SIZE = 64;
    char buffer[BUFFER_SIZE];

    int fd = open("sample.txt", O_WRONLY | O_CREAT, 0644);
    if(fd < 0) {
        return 1;
    }

    const char data[] = "hello geeks\n";
    const int dataLen = 12;

    int writtenSize = write(fd, data, dataLen);

    // might be skipped, due to end of program
    close(fd);

    return 0;
}
```


| Return value ||
| ------- | - |
| success | the number of bytes written is returned (zero indicates nothing was written) |
| error   | -1 and sets errno |



## [Lseek](https://linux.die.net/man/2/lseek)

reposition read/write file offset

```cpp
#include <sys/types.h>
#include <unistd.h>

off_t lseek(int fd, off_t offset, int whence);


int main() {


    return 0;
}
```

| Return value ||
| ------- | - |
| success | the resulting offset location as measured in bytes from the beginning of the file |
| error   | -1 and sets errno |



## [Dup](https://linux.die.net/man/2/dup)

These system calls create a copy of the file descriptor oldfd.
- `dup()` uses the lowest-numbered unused descriptor for the new descriptor.
- `dup2()` makes newfd be the copy of oldfd, closing newfd first if necessary.

```cpp
#include <unistd.h>

int dup(int oldfd);
int dup2(int oldfd, int newfd);
int dup3(int oldfd, int newfd, int flags); // is Linux-specific.


int main () {
    int fd = open("baz.txt", O_WRONLY | O_CREAT, 0644);
    if(fd < 0) {
        return 1;
    }

    close(STDOUT_FILENO);
    dup(fd);

    write(STDOUT_FILENO, "Hallo here\n", 11);
    write(fd, "Hallo here 2\n", 13);


    dup2(fd, STDERR_FILENO);
    write(STDERR_FILENO, "Hallo here error\n", 17);
    write(fd, "Hallo here 2 error\n", 19);

    int fd2 = dup(STDOUT_FILENO);
    write(fd2, "Hallo here from fd2\n", 20);

    return 0;
}
```

| Return value ||
| ------- | - |
| success | the new descriptor |
| error   | -1 and sets errno |



## [Sprintf](https://linux.die.net/man/3/sprintf)


```cpp
#include <stdio.h>

int printf(const char *format, ...);
int fprintf(FILE *stream, const char *format, ...);
int sprintf(char *str, const char *format, ...);
int snprintf(char *str, size_t size, const char *format, ...);
```

| Return value ||
| ------- | - |
| success | the number of characters printed (excluding the null byte used to end output to strings) |
| error   | a negative value |



## [Pipe](https://linux.die.net/man/2/pipe)

`pipe()` creates a pipe, a unidirectional data channel that can be used for interprocess communication. The array pipefd is used to return two file descriptors referring to the ends of the pipe. `pipefd[0]` refers to the read end of the pipe. `pipefd[1]` refers to the write end of the pipe. Data written to the write end of the pipe is buffered by the kernel until it is read from the read end of the pipe

```cpp
#include <unistd.h>
int pipe(int pipefd[2]);
int pipe2(int pipefd[2], int flags);

int main(int argc, char *argv[]) {
    int pipefd[2];
    pid_t cpid;
    char buf;
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <string>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }
    cpid = fork();
    if (cpid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if (cpid == 0) {    /* Child reads from pipe */
        close(pipefd[1]);          /* Close unused write end */
        while (read(pipefd[0], &buf, 1) > 0)
            write(STDOUT_FILENO, &buf, 1);
        write(STDOUT_FILENO, "\n", 1);
        close(pipefd[0]);
        _exit(EXIT_SUCCESS);
    } else {            /* Parent writes argv[1] to pipe */
        close(pipefd[0]);          /* Close unused read end */
        write(pipefd[1], argv[1], strlen(argv[1]));
        close(pipefd[1]);          /* Reader will see EOF */
        wait(NULL);                /* Wait for child */
        exit(EXIT_SUCCESS);
    }
}
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |



## [Wait](https://linux.die.net/man/2/wait)

wait for process to change state

```cpp
#include <sys/types.h>
#include <sys/wait.h>

pid_t wait(int *status);
pid_t waitpid(pid_t pid, int *status, int options);
```

| Return value ||
| ------- | - |
| success | the process ID of the terminated child |
| error   | -1 and sets errno |



## [Stat](https://linux.die.net/man/2/stat)

stat, fstat, lstat - get file status

```cpp
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int stat(const char *path, struct stat *buf);
int fstat(int fd, struct stat *buf);
int lstat(const char *path, struct stat *buf);

int main(int argc, char *argv[]) {
  struct stat sb;

  if (argc != 2) {
    fprintf(stderr, "Usage: %s <pathname>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  if (stat(argv[1], &sb) == -1) {
    perror("stat");
    exit(EXIT_FAILURE);
  }

  printf("File type:                ");

  switch (sb.st_mode & S_IFMT) {
    case S_IFBLK:
      printf("block device\n");
      break;
    case S_IFCHR:
      printf("character device\n");
      break;
    case S_IFDIR:
      printf("directory\n");
      break;
    case S_IFIFO:
      printf("FIFO/pipe\n");
      break;
    case S_IFLNK:
      printf("symlink\n");
      break;
    case S_IFREG:
      printf("regular file\n");
      break;
    case S_IFSOCK:
      printf("socket\n");
      break;
    default:
      printf("unknown?\n");
      break;
  }

  printf("I-node number:            %ld\n", (long)sb.st_ino);

  printf("Mode:                     %lo (octal)\n", (unsigned long)sb.st_mode);
  if (stats.st_mode & R_OK) printf("read ");
  if (stats.st_mode & W_OK) printf("write ");
  if (stats.st_mode & X_OK) printf("execute");

  printf("Link count:               %ld\n", (long)sb.st_nlink);
  printf("Ownership:                UID=%ld   GID=%ld\n", (long)sb.st_uid,
         (long)sb.st_gid);

  printf("Preferred I/O block size: %ld bytes\n", (long)sb.st_blksize);
  printf("File size:                %lld bytes\n", (long long)sb.st_size);
  printf("Blocks allocated:         %lld\n", (long long)sb.st_blocks);

  printf("Last status change:       %s", ctime(&sb.st_ctime));
  printf("Last file access:         %s", ctime(&sb.st_atime));
  printf("Last file modification:   %s", ctime(&sb.st_mtime));

  exit(EXIT_SUCCESS);
}
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |



## [Chmod](https://linux.die.net/man/2/chmod)

change permissions of a file

```cpp
#include <sys/stat.h>

int chmod(const char *path, mode_t mode);
int fchmod(int fd, mode_t mode);
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |



## [Chown](https://linux.die.net/man/2/chown)

change ownership of a file

```cpp
#include <unistd.h>

int chown(const char *path, uid_t owner, gid_t group);
int fchown(int fd, uid_t owner, gid_t group);
int lchown(const char *path, uid_t owner, gid_t group);
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |



## [Utime](https://linux.die.net/man/2/utime)

change file last access and modification times

```cpp
#include <sys/types.h>
#include <utime.h>

int utime(const char *filename, const struct utimbuf *times);

#include <sys/time.h>

int utimes(const char *filename, const struct timeval times[2]);
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |



## [Fcntl](https://linux.die.net/man/2/fcntl)

manipulate file descriptor

```cpp
#include <unistd.h>
#include <fcntl.h>

int fcntl(int fd, int cmd, ... /* arg */ );
```

| Result | Operation | Return value |
| ------- | --------- | - |
| success | F_DUPFD | The new descriptor. |
| success | F_GETFD | Value of file descriptor flags. |
| success | F_GETFL | Value of file status flags. |
| success | F_GETLEASE | Type of lease held on file descriptor. |
| success | F_GETOWN | Value of descriptor owner. |
| success | F_GETSIG | Value of signal sent when read or write becomes possible, or zero for traditional SIGIO behavior. |
| success | F_GETPIPE_SZ | The pipe capacity. |
| success | all other | 0 |
| error   | all | -1 and sets errno |



## [Link](https://linux.die.net/man/2/link)

make a new name for a file

```cpp
#include <unistd.h>

int link(const char *oldpath, const char *newpath);
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |



## [Unlink](https://linux.die.net/man/2/unlink)

delete a name and possibly the file it refers to

```cpp
#include <unistd.h>

int unlink(const char *pathname);
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |



## [Symlink](https://linux.die.net/man/2/symlink)

make a new name for a file

```cpp
#include <unistd.h>

int symlink(const char *oldpath, const char *newpath);
```

| Return value ||
| ------- | - |
| success | 0 |
| error   | -1 and sets errno |



## [Socket](https://linux.die.net/man/2/socket)

create an endpoint for communication

```cpp
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>

int socket(int domain, int type, int protocol);
```

| Return value ||
| ------- | - |
| success | a file descriptor for the new socket |
| error   | -1 and sets errno |
